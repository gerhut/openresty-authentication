module.exports = (req, res) => {
  if (req.body.user) {
    return res.set('X-User', req.body.user).sendStatus(204)
  } else {
    return res.sendStatus(400)
  }
}
