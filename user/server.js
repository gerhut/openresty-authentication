const express = require('express')

const app = module.exports = express()

app.get('/', require('./index'))
app.post('/login',
  express.urlencoded({ extended: false }),
  require('./login'))

if (require.main === module) {
  app.listen(process.env.PORT)
}
