module.exports = (req, res) => {
  const user = req.get('X-User')
  if (user) {
    return res.type('text').send(user)
  } else {
    return res.sendStatus(401)
  }
}
